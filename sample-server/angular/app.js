var app= angular.module('recipeApp',[]);

app.controller('recipeCtrl',function($scope,$http){
  $scope.recipe={}
  $scope.clearRecipe=function(){
    $scope.recipe={}
  }
  $scope.getRecipe=function(){
    $http.get('/recipe')
    .success(function(data){
      console.log(data);
      $scope.recipe=data;
    })
  }
})


app.directive('recipeDirective',function(){
  return {
    controller:'recipeCtrl',
    templateUrl:'/recipe-template.html'
  }
})
