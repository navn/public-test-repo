var express = require('express');
var path = require('path');
var app = express();
app.use(require('morgan')());

app.get('/',function(req,res){
  res.send("Hello from node!")
  // res.sendFile(path.resolve(__dirname+'/../client-side/index.html'))
});


// app.use(express.static('../client-side', { maxAge: 4 * 60 * 60 * 1000 /* 2hrs */ }));
// app.use(express.static('./uploads', { maxAge: 4 * 60 * 60 * 1000 /* 2hrs */ }));


var port=3030
app.listen(port);
console.log('magic happens ate port ',port);
